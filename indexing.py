
from utils import Utils, News, NewsDoc,\
     Twitter, TwitterLinks, TweetDoc, Elastic, Lang

from crawl_latest import TwitterLatest, DeskCoinLatest, MoneyNetLatest, BlockmediaLatest,\
    CcnLatest, CoinnessLatest, CoinDeskKoreaLatest, BlockChainHubLatest, TokenPostLatest

import dateparser
from elasticsearch import Elasticsearch

import os
import json
import datetime


FOLDER_NEWS = 'parsed_articles'

class ElasticS:

    def __init__(self, clean_instance=False, use_twitter=True, use_news=True):
        if clean_instance == False:
            self.es = self.get_elastic_instance()
        else:
            self.es = self.get_clean_elastic_instance()
        # get onjects for crawling the latest news / tweets
        self.news_obj_latest = []
        self.tweets_obj_latest = []
        self.driver = Utils.get_firefox_driver()
        #self.driver.set_page_load_timeout(20)
        
        # twitter latest objects
        if use_twitter == True:
            for twitterChannel in Twitter:
                for twitterLink in TwitterLinks:
                    if twitterChannel.name == twitterLink.name:
                        twitter = TwitterLatest(self.driver, twitterChannel, twitterLink.value)
                        self.tweets_obj_latest.append(twitter)
        # news latest bojects
        if use_news == True:
            blk_media_latest = BlockmediaLatest(self.driver)
            self.news_obj_latest.append(blk_media_latest)

            blkChain = BlockChainHubLatest(self.driver)
            self.news_obj_latest.append(blkChain)

            token_post = TokenPostLatest(self.driver)
            self.news_obj_latest.append(token_post)

            coin_desk_korea = CoinDeskKoreaLatest(self.driver)
            self.news_obj_latest.append(coin_desk_korea)

            ccn_latest = CcnLatest(self.driver)
            self.news_obj_latest.append(ccn_latest)

            coinness = CoinnessLatest(self.driver)
            self.news_obj_latest.append(coinness)

            desk_coin = DeskCoinLatest(self.driver)
            self.news_obj_latest.append(desk_coin)

            moneyneynet = MoneyNetLatest(self.driver)
            self.news_obj_latest.append(moneyneynet)

    def get_clean_elastic_instance(self):
        es = Elasticsearch([{'host': Elastic.ELASTIC_HOST.value, 'port': Elastic.ELASTIC_PORT.value}])
        print('[Elasticsearch] Connected to es server on host: {} and port: {}'.format(Elastic.ELASTIC_HOST.value, Elastic.ELASTIC_PORT.value))

        if es.indices.exists(index=Elastic.ELASTIC_INDEX_NEWS.value) == True:
            es.indices.delete(index=Elastic.ELASTIC_INDEX_NEWS.value, ignore=[400, 404])
            es.indices.create(index=Elastic.ELASTIC_INDEX_NEWS.value, ignore=400)
            es.indices.refresh(index=Elastic.ELASTIC_INDEX_NEWS.value)
        else:
            es.indices.create(index=Elastic.ELASTIC_INDEX_NEWS.value, ignore=400)
            print('[Elasticsearch] index {} created'.format(Elastic.ELASTIC_INDEX_NEWS.value))
        
        if es.indices.exists(index=Elastic.ELASTIC_INDEX_TWEETS.value) == True:
            es.indices.delete(index=Elastic.ELASTIC_INDEX_TWEETS.value, ignore=[400, 404])
            es.indices.create(index=Elastic.ELASTIC_INDEX_TWEETS.value, ignore=400)
            es.indices.refresh(index=Elastic.ELASTIC_INDEX_TWEETS.value)
        else:
            es.indices.create(index=Elastic.ELASTIC_INDEX_TWEETS.value, ignore=400)
            print('[Elasticsearch] Index {} created'.format(Elastic.ELASTIC_INDEX_TWEETS.value))
        return es

    def get_elastic_instance(self):
        es = Elasticsearch([{'host': Elastic.ELASTIC_HOST.value, 'port': Elastic.ELASTIC_PORT.value}])
        print('[Elasticsearch] Connected to es server on host: {} and port: {}'.format(Elastic.ELASTIC_HOST.value, Elastic.ELASTIC_PORT.value))

        if es.indices.exists(index=Elastic.ELASTIC_INDEX_NEWS.value) == True:
            es.indices.refresh(index=Elastic.ELASTIC_INDEX_NEWS.value)
        else:
            es.indices.create(index=Elastic.ELASTIC_INDEX_NEWS.value, ignore=400)
            print('[Elasticsearch] index {} created'.format(Elastic.ELASTIC_INDEX_NEWS.value))
        
        if es.indices.exists(index=Elastic.ELASTIC_INDEX_TWEETS.value) == True:
            es.indices.refresh(index=Elastic.ELASTIC_INDEX_TWEETS.value)
        else:
            es.indices.create(index=Elastic.ELASTIC_INDEX_TWEETS.value, ignore=400)
            print('[Elasticsearch] Index {} created'.format(Elastic.ELASTIC_INDEX_TWEETS.value))
        return es

    # return None to discard because no available date found
    def get_uniform_date(self, news_source, string_date):

        if news_source.value == News.BLKMEDIA.value:
            dt = dateparser.parse("".join(list(filter(lambda x: x.isdigit() or x == ' ', string_date))))
        else:
            dt = dateparser.parse(string_date)

        if dt == None:
            return None
        else:
            return dt.isoformat()

    def get_elastic_tweet_doc(self, tweet_channel, f):
        es_doc = {
            TweetDoc.SOURCE.value: tweet_channel.value, 
            TweetDoc.DATE.value: '',
            TweetDoc.AUTHOR.value: '',
            TweetDoc.COMMENTS.value: '',
            TweetDoc.RETWEETS.value: '',
            TweetDoc.LIKES.value: '',
            TweetDoc.LANG.value: Lang.ENG.value,
            TweetDoc.CONTENT.value: '',
            TweetDoc.LINK.value: '',
        }

        line_index = 0
        index_doc = True
        content_list = []

        for line in f:
            # date
            if line_index == 0:
                dt = dateparser.parse(line.strip(' \n')).isoformat()
                if dt == None:
                    index_doc = False
                    break
                es_doc[TweetDoc.DATE.value] = dt
            # author
            elif line_index == 1:
                if line.strip(' \n') != "None":
                    es_doc[TweetDoc.AUTHOR.value] = line.strip(' \n')
            # link
            elif line_index == 2:
                if line.strip(' \n')[0:4] == 'http':
                    es_doc[NewsDoc.LINK.value] = line.strip(' \n')
                else:
                    index_doc = False
                    break
            # comments number
            elif line_index == 3:
                es_doc[TweetDoc.COMMENTS.value] = int(line)
            # retweets number
            elif line_index == 4:
                es_doc[TweetDoc.RETWEETS.value] = int(line)
            # likes
            elif line_index == 5:
                es_doc[TweetDoc.LIKES.value] = int(line)
            # content
            else:
                content_list.append(line)
            line_index += 1
        
        content = "".join(content_list)
        if len(content) > 0 and index_doc == True:
            es_doc[TweetDoc.CONTENT.value] = content
            return es_doc
        return None

    def get_elastic_news_doc(self, news_source, f):
        
        es_doc = {NewsDoc.SOURCE.value: news_source.value,\
                  NewsDoc.TITLE.value: '',\
                  NewsDoc.AUTHOR.value: '',\
                  NewsDoc.CONTENT.value: '',\
                  NewsDoc.DATE.value: '',\
                  NewsDoc.LINK.value: '',\
                  NewsDoc.LANG.value: Lang.KO.value}
        
        if news_source.value == News.CCN.value or news_source.value == News.DESKCOIN.value:
            es_doc[NewsDoc.LANG.value] = Lang.ENG.value
        # if should index the doc or not
        index_doc = True
        line_index = 0
        content_list = []

        for line in f:
            # date
            if line_index == 0:
                dt = self.get_uniform_date(news_source, line.strip())
                if dt == None:
                    index_doc = False
                    break
                es_doc[NewsDoc.DATE.value] = dt
            # author
            elif line_index == 1:
                if line.strip(' \n') != "None":
                    es_doc[NewsDoc.AUTHOR.value] = line.strip(' \n')
            # title
            elif line_index == 2:
                if line.strip(' \n') != "None":
                    es_doc[NewsDoc.TITLE.value] = line.strip(' \n')
            # link
            elif line_index == 3:
                if line.strip(' \n')[0:4] == 'http':
                    es_doc[NewsDoc.LINK.value] = line.strip(' \n')
                else:
                    index_doc = False
                    break
            # content
            else:
                content_list.append(line)
            line_index += 1
        
        content = "".join(content_list)
        if len(content) > 0 and index_doc == True:
            es_doc[NewsDoc.CONTENT.value] = content
            return es_doc
        return None

    # index one twitter channel
    def index_tweet_channel_from_dir(self, tweet_channel):
        print('[Elasticsearch] Started indexing docs for index {} and source: {} '\
        .format(Elastic.ELASTIC_INDEX_TWEETS, tweet_channel.value))

        example_doc =  {
            NewsDoc.SOURCE.value: tweet_channel.value,
            NewsDoc.DATE.value: 'date',
            NewsDoc.AUTHOR.value: 'author',
            NewsDoc.TITLE.value: 'title',
            NewsDoc.CONTENT.value: 'content'
        }
        
        dirPages = os.path.join('all', 'twitter', tweet_channel.value)
        file_tweets = [os.path.join(dirPages, f) for f in os.listdir(dirPages) if os.path.isfile(os.path.join(dirPages, f))]
        
        # each file is a doc in es
        for tweet in file_tweets:
            # index doc if everything is alright
            es_doc = None
            try:
                with open(tweet, 'r', encoding='utf-8') as f:
                    es_doc = self.get_elastic_tweet_doc(tweet_channel, f)
                if es_doc != None:
                    self.es.index(index=Elastic.ELASTIC_INDEX_TWEETS.value,\
                     doc_type=Elastic.ELASTIC_DOC_TYPE_TWEET.value, body=es_doc)
            except:
                print('[Elasticsearch] Failed to index tweet {} in channel {}'
                .format(tweet, tweet_channel.value))

        self.es.indices.refresh(index=Elastic.ELASTIC_INDEX_TWEETS.value)
        print('[Elasticsearch] Finished indexing docs for index {} channel {}'\
        .format(Elastic.ELASTIC_INDEX_TWEETS.value, tweet_channel.value))

    # index one news source (type News)
    def index_source_news_from_dir(self, news_source):
        print('[Elasticsearch] Started indexing docs for index {} and source: {} '\
        .format(Elastic.ELASTIC_INDEX_NEWS, news_source.value))

        example_doc =  {
            NewsDoc.SOURCE.value: news_source.value,
            NewsDoc.DATE.value: 'date',
            NewsDoc.AUTHOR.value: 'author',
            NewsDoc.TITLE.value: 'title',
            NewsDoc.CONTENT.value: 'content',
            NewsDoc.LINK.value: 'link'
        }
        
        dirPages = os.path.join("all", news_source.value, FOLDER_NEWS)
        file_news = [os.path.join(dirPages, f) for f in os.listdir(dirPages) if os.path.isfile(os.path.join(dirPages, f))]
        
        # each file is a doc in es
        for news in file_news:
            # index doc if everything is alright
            es_doc = None
            try:
                with open(news, 'r', encoding='utf-8') as f:
                    es_doc = self.get_elastic_news_doc(news_source, f)
                if es_doc != None:
                    self.es.index(index=Elastic.ELASTIC_INDEX_NEWS.value,\
                        doc_type=Elastic.ELASTIC_DOC_TYPE_NEWS.value, body=es_doc)
            except:
                print('[Elasticsearch] Failed indexing for doc {} in source {}'.format(news, news_source.value))
        self.es.indices.refresh(index=Elastic.ELASTIC_INDEX_NEWS.value)
        print('[Elasticsearch] Finished indexing docs for index {} source {}'\
        .format(Elastic.ELASTIC_INDEX_NEWS.value, news_source.value))

    # for query ranges & dates formats:
    # https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-range-query.html
    def search_news(self, news_source=None, verbose=False):
        self.es.indices.refresh(index=Elastic.ELASTIC_INDEX_NEWS.value)

        search_query = {
            "from": 0, 
            "size": 30, # max limit is 10 000, default size=10
            "sort": [{"date": {'order': 'desc'}}], # order desc
            "query": {
                "bool": {
                    "must": [ {
                        "range" : {
                            "date" : {
                                "gte" : "now-2d",
                                "lt" :  "now"
                            }
                        }} ,
                        #{"match": {"source": "tokenpost"} } # if you want to search only in one news source
                    ]  
                }
            }
        }
        if news_source != None:
            search_query["query"]["bool"]["must"].append({"match": {"source": news_source.value}})

        query_res = self.es.search(index=Elastic.ELASTIC_INDEX_NEWS.value, body=search_query)
        if verbose == True:
            print(json.dumps(query_res, indent=True))
        return query_res

    def search_tweets(self, twitter_channel=None, verbose=False):
        self.es.indices.refresh(index=Elastic.ELASTIC_INDEX_TWEETS.value)

        search_query = {
            "from": 0, 
            "size": 30, # max limit is 10 000, default size=10
            "sort": [{"date": {'order': 'desc'}}], # order desc
            "query": {
                "bool": {
                    "must": [ {
                        "range" : {
                            "date" : {
                                "gte" : "now-10d",
                                "lt" :  "now"
                            }
                        }} ,
                        #{"match": {"source": "twitter_channel.value"} } # if you want to search only in one news source
                    ]  
                }
            }
        }
        if twitter_channel != None:
            search_query["query"]["bool"]["must"].append({"match": {"source": twitter_channel.value}})

        query_res = self.es.search(index=Elastic.ELASTIC_INDEX_TWEETS.value, body=search_query)
        if verbose == True:
            print(json.dumps(query_res, indent=True))
        return query_res

    # get latest new, source can be a twitter channel or website
    def search_last_indexed(self, index_to_seacrh=Elastic.ELASTIC_INDEX_NEWS,\
         source=None, size=1, verbose=False):
        search_query = {
            "from": 0, 
            "size": size, # max limit is 10 000, default size=10
            "sort": [{"date": {'order': 'desc'}}], # order desc
            "query": {
                "bool": {
                    "must": [ 
                        #{"match": {"source": "tokenpost"} } # if you want to search only in one news source
                    ]  
                }
            }
        }
        if source != None:
            search_query["query"]["bool"]["must"].append({"match": {"source": source.value}})

        query_res = self.es.search(index=index_to_seacrh.value, body=search_query)
        print('[Elasticsearch] Getting latest article already indexed for {}, source: {}'\
        .format(index_to_seacrh.value, source.value))
        if verbose == True:
            print(json.dumps(query_res, indent=True))
        return query_res
    
    # datetime, construct datetime.datetime(year, month, day)
    # lang is "ko" or "eng"
    def search_date_interval(self, start_date, end_date,\
        index_to_seacrh=Elastic.ELASTIC_INDEX_NEWS, verbose=False, lang=Lang.KO):
        
        start_date_string = '{}/{}/{}'.format(start_date.year, start_date.month, start_date.day)
        end_date_string = '{}/{}/{}'.format(end_date.year, end_date.month, end_date.day)
       
        search_query = {
            "from": 0, 
            "sort": [{"date": {'order': 'desc'}}], # order desc
            "size": 10000,
            "query": {
                "bool": {
                    "must": [ {
                            "range" : {
                                "date" : {
                                    "gte" : start_date_string,
                                    "lte" : end_date_string,
                                    "format": "yyyy/MM/dd"
                                }
                            }
                        },{
                            "match": {
                                "language": lang.value
                            } 
                        }
                    ]
                }
            }
        }
        #if source != None:
        #    search_query["query"]["bool"]["must"].append({"match": {"source": source.value}})

        query_res = self.es.search(index=index_to_seacrh.value, body=search_query)
        print('[Elasticsearch] Getting articles between {} and {} (sorted) already indexed for {}'\
        .format(start_date_string, end_date_string, index_to_seacrh.value))
        if verbose == True:
            print(json.dumps(query_res, indent=True))
        return query_res

    def index_news_from_dir(self):
        for news_source in News:
            self.index_source_news_from_dir(news_source)

    def index_tweets_from_dir(self):
        for tweet_channel in Twitter:
            self.index_tweet_channel_from_dir(tweet_channel)

    def index_news_from_memory(self, articles, news_source):
        print('[Elasticsearch] Started indexing {} latest news for index {} for source {}'\
        .format(len(articles), Elastic.ELASTIC_INDEX_NEWS, news_source.value))

        for article in articles:
            self.es.index(index=Elastic.ELASTIC_INDEX_NEWS.value,\
                doc_type=Elastic.ELASTIC_DOC_TYPE_NEWS.value, body=article)
        
        print('[Elasticsearch] Finished indexing {} latest news for index {} for source {}'\
        .format(len(articles), Elastic.ELASTIC_INDEX_NEWS.value, news_source.value))

    def index_tweets_from_memory(self, tweets, twitter_channel):
        print('[Elasticsearch] Started indexing {} latest tweets for index {} for source {}'\
        .format(len(tweets), Elastic.ELASTIC_DOC_TYPE_TWEET, twitter_channel.value))

        for tweet in tweets:
            self.es.index(index=Elastic.ELASTIC_INDEX_TWEETS.value,\
                doc_type=Elastic.ELASTIC_DOC_TYPE_TWEET.value, body=tweet)
        
        print('[Elasticsearch] Finished indexing {} latest tweets for index {} for source {}'\
        .format(len(tweets), Elastic.ELASTIC_INDEX_TWEETS.value, twitter_channel.value))

    # crawling and indexing latest tweets for each channel
    def crawl_and_index_latest_twitter(self):
        for obj_latest_twitter in self.tweets_obj_latest:
            last_indexed = elastic.search_last_indexed(index_to_seacrh=Elastic.ELASTIC_INDEX_TWEETS,\
                                                        source=obj_latest_twitter.source,
                                                        size=1,
                                                        verbose=True)
            dt = Utils.get_latest_date(last_indexed)

            latest_tweets = obj_latest_twitter.get_latest_articles(dt)
            elastic.index_tweets_from_memory(latest_tweets, obj_latest_twitter.source)
            elastic.search_last_indexed(index_to_seacrh=Elastic.ELASTIC_INDEX_TWEETS,\
                                        source=obj_latest_twitter.source,
                                        size=1,
                                        verbose=True)
    
    # crawling and indexing latest news for each website
    def crawl_and_index_latest_sites(self):
        for obj_latest_new in self.news_obj_latest:
            last_indexed = elastic.search_last_indexed(index_to_seacrh=Elastic.ELASTIC_INDEX_NEWS,\
                                                        source=obj_latest_new.source,
                                                        size=1,
                                                        verbose=True)
            dt = Utils.get_latest_date(last_indexed)
            latest_news = obj_latest_new.get_latest_articles(dt)
            elastic.index_news_from_memory(latest_news, obj_latest_new.source)
            elastic.search_last_indexed(index_to_seacrh=Elastic.ELASTIC_INDEX_NEWS,\
                                        source=obj_latest_new.source,
                                        size=5,
                                        verbose=True)

if __name__ == '__main__':
    elastic = ElasticS(clean_instance=False, use_twitter=True, use_news=True)
    #elastic.index_news_from_dir()
    #elastic.search_last_news(news_source=News.COINKO)
    #elastic.search_news()
    #print('Done indexing news')
    #elastic.crawl_and_index_latest_twitter()
    #elastic.crawl_and_index_latest_sites()
    #elastic.search_tweets()
    elastic.search_date_interval(datetime.datetime(2018, 11, 3), \
    datetime.datetime(2018, 12, 6), verbose=1, lang=Lang.KO)
    elastic.driver.quit()