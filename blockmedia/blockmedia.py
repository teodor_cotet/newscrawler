from selenium import webdriver
import selenium
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options
from pyquery import PyQuery as pq
import dateutil.parser
from datetime import datetime
from dateutil.tz import tzutc
from os import listdir
from os.path import isfile, join

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import time
class Blockmedia:
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.baseLink = 'https://www.blockmedia.co.kr/'
        print("Headless Firefox Initialized")

    def getPages(self, resFolder):
        self.driver.get(self.baseLink)
        
        for i in range(1, 247):
            try:
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
                #wait = WebDriverWait(self.driver, 40)
                #element = wait.until(EC.lement_to_be_clickable((By.CSS_SELECTOR, '.pagination .pagination-page-{}'.format(i))))
                #element = WebDriverWait(self.driver, 10).until(lambda x: x.find_element_by_css_selector('.pagination .pagination-page-{}'.format(i)))
                for j in range(i, i + 10):
                    #attr_style = self.driver.find_elements_by_css_selector('.pagination li')[j].get_attribute("style")
                    attr_class = self.driver.find_elements_by_css_selector('.pagination li')[j].get_attribute("class")
                    if attr_class == "":
                        time.sleep(5)
                        element = self.driver.find_elements_by_css_selector('.pagination li')[j]
                        #self.driver.execute_script("arguments[0].setAttribute('style','display: block;')", element)
                        #attr = self.driver.find_elements_by_css_selector('.pagination li')[i].get_attribute("style")
                        #print(attr)
                        #element = self.driver.find_elements_by_css_selector('.pagination li')[i]
                        element.click()
                        print(i)
                        with open(resFolder + '/page' + str(i) + '.txt', 'w', encoding='utf-8') as f:
                            f.write(self.driver.page_source)
                        break
            except:
                print('failed')

    def getArticles(self, dirPages, outFolder):
        onlyfiles = [f for f in listdir(dirPages) if isfile(join(dirPages, f))]
        j = 0

        inFile = 'page176.txt'
        with open(dirPages + '/' + inFile, 'r', encoding='utf-8') as f:
            s = f.read()
            
        p1 = pq(s)
        nrArticles = len(p1('.paginated_content article .post-content .entry-title a'))
        if True:
            for i in range(nrArticles):
                try:
                    link = p1('.paginated_content article .post-content .entry-title a').eq(i).attr('href')
                    print(link)
                    self.driver.get(link)
                    p2 = pq(self.driver.page_source)
                    
                    with open(outFolder + '/' + str(j) + '.txt', 'w', encoding='utf-8') as f:
                        # date 
                        f.write(p2('article .post-header .post-meta span').eq(0).text())
                        f.write('\n')
                        # author
                        f.write(p2('article .post-header .post-meta p a').eq(0).text())
                        f.write('\n')
                        # title
                        f.write(p2('article .post-header .entry-title').text())
                        f.write('\n')
                        # link
                        f.write(link)
                        f.write('\n')
                        # content
                        nPar = len(p2('article .post-wrap .entry-content p'))
                        content = "\n".join([p2('article .post-wrap .entry-content p').eq(i).text() for i in range(nPar)])
                        f.write(content)
                        j += 1
                except:
                    print('failed ' + str(i))
            self.driver.quit()

if __name__ == "__main__":
    blockmedia = Blockmedia()
    blockmedia.getArticles('pages', 'parsed_articles')
    #dk.getLinks('res.html')
    #blockmedia.getPages('pages')
