from selenium import webdriver
import selenium
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options
from pyquery import PyQuery as pq
import dateutil.parser
from datetime import datetime
from dateutil.tz import tzutc

class DeskCoin:
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.baseLink = 'https://www.coindesk.com/'
        print("Headless Firefox Initialized")

    def getLinks(self, resFile):
        self.driver.get(self.baseLink)
        dt_last_article = datetime(2017, 1, 1, tzinfo=tzutc())
        
        while True:
            self.driver.find_element_by_css_selector('#load-more-stories button').click()
            p = pq(self.driver.page_source)
            n = len(p('.stream-article time'))
            dt = p('.stream-article time').eq(n-1).attr('datetime')
            parsed_dt = dateutil.parser.parse(dt)
            print(n, parsed_dt, dt_last_article)
            if parsed_dt < dt_last_article:
                with open(resFile, 'w', encoding='utf-8') as f:
                    f.write(self.driver.page_source)
                self.driver.close()
                self.driver.quit()
                break

    def getArticles(self, inFile, outFolder):
        with open(inFile, 'r', encoding='utf-8') as f:
            s = f.read()
            
        p1 = pq(s)
        articles = p1('.stream-article')
        n = len(articles)

        for i in range(n):
            try:
                link = articles.eq(i).attr['href']
                self.driver.get(link)
                p2 = pq(self.driver.page_source)

                # print(p2('.entry-content').text())
                # print(p2('.author-names').text())
                # print(p1('.stream-article time').eq(i).attr('datetime'))
                # print(p1('.stream-article').eq(i).attr['title'])

                with open(outFolder + '/' + str(i) + '.txt', 'w', encoding='utf-8') as f:
                    f.write(p1('.stream-article time').eq(i).attr('datetime'))
                    f.write('\n')
                    f.write(p2('.author-names').text())
                    f.write('\n')
                    f.write(p1('.stream-article').eq(i).attr['title'])
                    f.write('\n')
                    f.write(link)
                    f.write('\n')
                    f.write(p2('.entry-content').text())
            except:
                print('failed')
        self.driver.quit()

if __name__ == "__main__":
    dk = DeskCoin()
    dk.getArticles('res.html', 'parsed_articles')
    #dk.getLinks('res.html')
