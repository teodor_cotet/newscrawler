from selenium import webdriver
import selenium
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options
from pyquery import PyQuery as pq
import dateutil.parser
from datetime import datetime
from dateutil.tz import tzutc

class Ccn:
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.baseLink = 'https://www.ccn.com/'
        print("Headless Firefox Initialized")

    def getLinks(self, resFile):
        self.driver.get(self.baseLink)
        dt_last_article = datetime(2018, 6, 1, tzinfo=tzutc())
        
        while True:
            try:
                self.driver.find_element_by_css_selector('.posts-row .load-more-btn').click()
                p = pq(self.driver.page_source)
                n = len(p('.posts-row article time'))
                dt = p('.posts-row article time').eq(n-1).attr('datetime')
                parsed_dt = dateutil.parser.parse(dt)
                print(n, parsed_dt, dt_last_article)
                if parsed_dt < dt_last_article:
                    with open(resFile, 'w', encoding='utf-8') as f:
                        f.write(self.driver.page_source)
                    self.driver.close()
                    self.driver.quit()
                    break
            except:
                print('failed')

    def getArticles(self, inFile, outFolder):
        with open(inFile, 'r', encoding='utf-8') as f:
            s = f.read()
        p1 = pq(s)
        articles = p1('.posts-row article')
        n = len(articles)

        for i in range(n):
            try:
                link = articles.eq(i)('a').attr['href']
                self.driver.get(link)
                p2 = pq(self.driver.page_source)
                print(i, link)
                # print(p2('.entry-content').text())
                # print(p2('.author-names').text())
                # print(p1('.stream-article time').eq(i).attr('datetime'))
                # print(p1('.stream-article').eq(i).attr['title'])

                with open(outFolder + '/' + str(i) + '.txt', 'w', encoding='utf-8') as f:
                    # date
                    parseDate = p2('meta').filter(lambda x: pq(this).attr('property')=='article:published_time').eq(0).attr('content')
                    f.write(str(parseDate))
                    f.write('\n')
                    # author
                    f.write(str(p2('.page-content article footer .post-author').text().replace("AUTHOR", "")))
                    f.write('\n')
                    # title
                    title = p2('meta').filter(lambda x: pq(this).attr('property')=='og:title').eq(0).attr('content')
                    f.write(str(title))
                    f.write('\n')
                    # link
                    f.write(str(link))
                    f.write('\n')
                    # p
                    nPar = len(p2('article .entry-content p'))
                    content = "\n".join([p2('article .entry-content p').eq(tt).text() for tt in range(nPar)])
                    f.write(str(content))
            except:
                print('failed ' + str(i))
        self.driver.quit()

if __name__ == "__main__":
    dk = Ccn()
    dk.getArticles('res.html', 'parsed_articles')
    #dk.getLinks('res.html')
