from utils import Utils, News, NewsDoc,\
     Twitter, TwitterLinks, TweetDoc, Elastic, Lang


import selenium
from selenium import webdriver
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from pyquery import PyQuery as pq
import dateutil.parser
from datetime import datetime
from dateutil.tz import tzutc
from os import listdir
from os.path import isfile, join
import dateparser

import json
import pytz
import time

# twitter
class TwitterLatest:
    def __init__(self, driver, channel, base_link):
        # options = Options()
        # options.headless = True
        self.driver = driver
        self.baseLink = base_link
        self.source = channel
        print("[Crawl] Headless Firefox Initialized for {}, channel {}".format(type(self).__name__, channel.value))

    def get_latest_articles(self, dt_last_crawled_article, latest=200):

        print('[Crawl] Adding maximum {} tweets recent than {} for {}'\
        .format(latest, dt_last_crawled_article.isoformat(), self.source.value))
        all_new_articles = []
        dt_last_crawled_article = Utils.dt_make_aware(dt_last_crawled_article)
        nr_articles = 0
        try:
            self.driver.get(self.baseLink)
        except selenium.common.exceptions.TimeoutException:
            print("[Crawl] Timeout on baselink for {}, channel {}, 0 tweets indexed".format(type(self).__name__, channel.value))
            return []
        except:
            print("[Crawl] Timeout on baselink for {}, channel {}, 0 tweets indexed".format(type(self).__name__, channel.value))
            return []

        while nr_articles < latest:
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            p1 = pq(self.driver.page_source)
            tweetSel = '.ProfileTimeline ol li .tweet'
            tweets = p1(tweetSel)
            nr_articles = len(tweets)

        print('[Crawl] Tweets to be crawled: {} for {}'.format(nr_articles, self.source.value))
        
        lastTweetCrawled = 0
        while lastTweetCrawled < nr_articles:
            try:
                article = {}
                tweet = tweets.eq(lastTweetCrawled)
                # date 
                strTime = tweet('.content .time ._timestamp').attr('data-time')
                dt_article_iso = str(datetime.utcfromtimestamp(int(strTime)).isoformat())

                # author / tweet channel
                author = str(tweet('.content .stream-item-header .username').text())
                
                # link
                link = 'https://twitter.com' + str(tweet('.content .stream-item-header > small a').attr('href'))

                # comments
                selComments = tweet('.content .stream-item-footer .ProfileTweet-actionList .ProfileTweet-action--reply .ProfileTweet-actionCount .ProfileTweet-actionCountForPresentation').eq(0)
                if len(selComments.text()) == 0:
                    nrComments = 0
                else:
                    nrComments = int(selComments.text())
                    
                # retweets
                selReTweets = tweet('.content .stream-item-footer .ProfileTweet-actionList .ProfileTweet-action--retweet .ProfileTweet-actionCount .ProfileTweet-actionCountForPresentation').eq(0)
                #print(selReTweets.text())
                if len(selReTweets.text()) == 0:
                    retweets = 0
                else:
                    retweets = int(selReTweets.text())

                # # likes
                selLikes = tweet('.content .stream-item-footer .ProfileTweet-actionList .ProfileTweet-action--favorite .ProfileTweet-actionCount .ProfileTweet-actionCountForPresentation').eq(0)
                if len(selLikes.text()) == 0:
                    likes = 0
                else:
                    likes = int(selLikes.text())

                #no title for tweets
                # content
                cssSeelector = '.content .js-tweet-text-container p'
                nPar = len(tweet(cssSeelector))
                content = "".join([tweet(cssSeelector).eq(i).text() for i in range(nPar)])
                cc = " ".join(content.split('\n'))
                goodContent = cc.replace("@ ", "@").replace("# ", "#")
                content = goodContent

                article[TweetDoc.DATE.value] = dt_article_iso
                article[TweetDoc.AUTHOR.value] = author
                article[TweetDoc.CONTENT.value] = content
                article[TweetDoc.LANG.value] = Lang.ENG.value
                article[TweetDoc.SOURCE.value] = self.source.value
                article[TweetDoc.LINK.value] = link

                article[TweetDoc.LIKES.value] = likes
                article[TweetDoc.COMMENTS.value] = nrComments
                article[TweetDoc.RETWEETS.value] = retweets

                lastTweetCrawled += 1

                dt_article = dateparser.parse(article[TweetDoc.DATE.value])
                dt_article = Utils.dt_make_aware(dt_article)

                if dt_last_crawled_article < dt_article:
                    all_new_articles.append(article)
                    print('[Crawl] Article {} added with date-time: {} for {}'.format(link, dt_article.isoformat(), self.source.value))
            except:
                print('[Crawl] Tweet failed')
                lastTweetCrawled += 1
        print('[Crawl] {} recent tweets to be added for {}'.format(len(all_new_articles), self.source.value))
        return all_new_articles

# click based
class DeskCoinLatest:

    def __init__(self, driver):
        # options = Options()
        # options.headless = True
        self.driver = driver
        self.baseLink = 'https://www.coindesk.com/'
        self.source = News.DESKCOIN
        print("[Crawl] Headless Firefox Initialized for {}".format(type(self).__name__))

    def get_latest_articles(self, dt_last_crawled_article, latest=200):

        print('[Crawl] Adding maximum {} articles recent than {} for {}'.format(latest, dt_last_crawled_article.isoformat(), self.source.value))
        all_new_articles = []
        dt_last_crawled_article = Utils.dt_make_aware(dt_last_crawled_article)
        nr_articles = 0

        self.driver.get(self.baseLink)

        while nr_articles < latest:
            self.driver.find_element_by_css_selector('#load-more-stories button').click()
            p1 = pq(self.driver.page_source)
            select = '.content #article-streams .article-stream .stream-article'
            nr_articles = len(p1(select))

        print('[Crawl] Articles to be crawled: {} for {}'.format(nr_articles, self.source.value))

        for i in range(nr_articles):
            try:
                article, link = self.crawl_article(i, p1)
                dt_article = dateparser.parse(article[NewsDoc.DATE.value])
                dt_article = Utils.dt_make_aware(dt_article)

                if dt_last_crawled_article < dt_article:
                    all_new_articles.append(article)
                    print('[Crawl] Article {} added with date-time: {} for {}'.format(link, dt_article.isoformat(), self.source.value))
            except:
                print('[Crawl] Article {} failed for {}'.format(link, self.source.value))
        return all_new_articles

    # get index article from page p1
    def crawl_article(self, index, p1):
        article = {}
        link = p1('.content #article-streams .article-stream .stream-article').eq(index).attr('href')
        self.driver.get(link)
        p2 = pq(self.driver.page_source)
        
        # date 
        dt_raw = str(p1('.stream-article time').eq(index).attr('datetime'))
        dt_article = Utils.dt_make_aware(dateparser.parse(dt_raw))
        dt_article_iso = dt_article.isoformat()
        
        # author
        author = str(p2('.author-names').text())
        
        # title
        title = str(p1('.stream-article').eq(index).attr['title'])
        
        # content
        selContent = 'article .entry-content p'
        nPar = len(p2(selContent))
        content = "\n".join([p2(selContent).eq(tt).text() for tt in range(nPar)])

        article[NewsDoc.DATE.value] = dt_article_iso
        article[NewsDoc.AUTHOR.value] = author
        article[NewsDoc.TITLE.value] = title
        article[NewsDoc.CONTENT.value] = content
        article[NewsDoc.LANG.value] = Lang.ENG.value
        article[NewsDoc.SOURCE.value] = self.source.value
        article[NewsDoc.LINK.value] = link
        return article, link  

# page based
class MoneyNetLatest:
    def __init__(self, driver):
        # options = Options()
        # options.headless = True
        self.driver = driver
        self.baseLink = 'https://www.moneynet.co.kr/index.php?mid=info_board&category=20297&page='
        self.source = News.MONEYNEY
        print("[Crawl] Headless Firefox Initialized for {}".format(type(self).__name__))
    
    def get_latest_articles(self, dt_last_crawled_article):

        print("[Crawl] Getting latest articles ({}) for {}"\
        .format(dt_last_crawled_article.isoformat(), type(self).__name__))
        dt_last_crawled_article = Utils.dt_make_aware(dt_last_crawled_article)
        generator_pages = self.get_pages(max_pages=10)
        all_new_articles = []

        while True:
            page = next(generator_pages)
            articles = self.get_articles(page, dt_last_crawled_article)
            all_new_articles += articles
            print('[Crawl] New articles crawled {} for {}'.format(len(articles), type(self).__name__))
            
            if len(articles) == 0:
                print('[Crawl] Done crawling for {}, crawled {} articles'\
                .format(type(self).__name__, len(all_new_articles)))
                break
        
        return all_new_articles

    def get_pages(self, max_pages=10):
        
        for i in range(1, max_pages):
            try:
                link = self.baseLink + str(i)
                print('[Crawl] Getting page {} for {}'.format(link, type(self).__name__))
                self.driver.get(link)
                time.sleep(2)
                yield self.driver.page_source
            except GeneratorExit:
                print('[Crawl] Page generator ended for {}'.format(type(self).__name__))
                return
            except:
                print('[Craw] Failed page {} for {}'.format(link, type(self).__name__))

    def get_articles(self, page, dt_last_crawled_article):
        p1 = pq(page)
        sel = 'tbody tr .title a'
        nrArticles = len(p1(sel))
        print('[Crawl] Crawling {} articles (a page) for {}'.format(nrArticles,  type(self).__name__))
        all_new_articles = []

        for i in range(nrArticles):
            try:
                article = {}
                link = p1(sel).eq(i).attr('href')
                self.driver.get(link)
                time.sleep(4)
                print('[Crawl] Getting article {} for {}'.format(link, type(self).__name__))
                p2 = pq(self.driver.page_source)
                # date
                date_raw = p2('meta').filter(lambda x: pq(this).attr('property')=='article:published_time').eq(0).attr('content')
                date_parsed = dateparser.parse(date_raw)
                dt_article_iso = date_parsed.isoformat()
                # author
                author = str(p1('tbody tr .author').eq(i).text())
                # title
                title = str(p2('.board_read .section_bottom h1').text())
                # content
                nPar = len(p2('.section_wrap .read_body p'))
                content = "\n".join([p2('.section_wrap .read_body p').eq(tt).text() for tt in range(nPar)])

                article[NewsDoc.DATE.value] = dt_article_iso
                article[NewsDoc.AUTHOR.value] = author
                article[NewsDoc.TITLE.value] = title
                article[NewsDoc.CONTENT.value] = content
                article[NewsDoc.LINK.value] = link
                article[NewsDoc.LANG.value] = Lang.KO.value
                article[NewsDoc.SOURCE.value] = self.source.value

                dt_article = dateparser.parse(article[NewsDoc.DATE.value])
                dt_article = Utils.dt_make_aware(dt_article)
                if dt_article > dt_last_crawled_article and len(content) > 0:
                    print('[Crawl] Article date {} recent than {} for {}'\
                    .format(dt_article.isoformat(), dt_last_crawled_article.isoformat(), type(self).__name__))
                    all_new_articles.append(article)
            except:
                print('[Craw] Failed article {} for {}'.format(link, type(self).__name__))
        return all_new_articles

# page based - without generators
class BlockmediaLatest:

    def __init__(self, driver):
        # options = Options()
        # options.headless = True
        self.driver = driver
        self.baseLink = 'https://www.blockmedia.co.kr/'
        self.source = News.BLKMEDIA
        print("[Crawl] Headless Firefox Initialized for {}".format(type(self).__name__))

    def get_latest_articles(self, dt_last_crawled_article):

        print("[Crawl] Getting latest articles ({}) for {}"\
        .format(dt_last_crawled_article.isoformat(), type(self).__name__))
        dt_last_crawled_article = Utils.dt_make_aware(dt_last_crawled_article)
        all_new_articles = []

        pages = self.get_pages(7)
        page = pages[-1]

        articles = self.get_articles(page, dt_last_crawled_article)
        all_new_articles += articles
        print('[Crawl] Done crawling for {}, crawled {} articles'\
        .format(type(self).__name__, len(all_new_articles)))
        
        return all_new_articles

    def get_pages(self, pages_limit):

        self.driver.get(self.baseLink)
        pages = []
        for i in range(1, pages_limit):
            try:
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
                #wait = WebDriverWait(self.driver, 40)
                #element = wait.until(EC.lement_to_be_clickable((By.CSS_SELECTOR, '.pagination .pagination-page-{}'.format(i))))
                #element = WebDriverWait(self.driver, 10).until(lambda x: x.find_element_by_css_selector('.pagination .pagination-page-{}'.format(i)))
                for j in range(i, i + 10):
                    #attr_style = self.driver.find_elements_by_css_selector('.pagination li')[j].get_attribute("style")
                    attr_class = self.driver.find_elements_by_css_selector('.pagination li')[j].get_attribute("class")
                    if attr_class == "":
                        time.sleep(5)
                        element = self.driver.find_elements_by_css_selector('.pagination li')[j]
                        element.click()
                        pages.append(self.driver.page_source)
                        break
            except:
                print('[Crawl] Failed page for {}'.format(type(self).__name__))
        return pages
    # get index article from page p1
    def get_articles(self, page, dt_last_crawled_article):

        all_new_articles = []
        p1 = pq(page)
        nr_articles = len(p1('.paginated_content article .post-content .entry-title a'))

        for i in range(nr_articles):
            try:
                article = {}
                link = p1('.paginated_content article .post-content .entry-title a').eq(i).attr('href')
                self.driver.get(link)
                time.sleep(5)
                p2 = pq(self.driver.page_source)

                # date 
                dt_raw = p2('meta').filter(lambda x: pq(this).attr('property')=='article:published_time').eq(0).attr('content')
                dt_article = Utils.dt_make_aware(dateparser.parse(dt_raw))
                dt_article_iso = dt_article.isoformat()
                
                # author
                author = p2('meta').filter(lambda x: pq(this).attr('name')=='author').eq(0).attr('content')
                
                # title
                title = p2('meta').filter(lambda x: pq(this).attr('property')=='og:title').eq(0).attr('content')
                
                # content
                selContent = 'article .post-wrap .entry-content p' 
                nPar = len(p2(selContent))
                content = "\n".join([p2(selContent).eq(tt).text() for tt in range(nPar)])

                article[NewsDoc.DATE.value] = dt_article_iso
                article[NewsDoc.AUTHOR.value] = author
                article[NewsDoc.TITLE.value] = title
                article[NewsDoc.CONTENT.value] = content
                article[NewsDoc.LANG.value] = Lang.KO.value
                article[NewsDoc.SOURCE.value] = self.source.value
                article[NewsDoc.LINK.value] = link

                dt_article = dateparser.parse(article[NewsDoc.DATE.value])
                dt_article = Utils.dt_make_aware(dt_article)
                if dt_article > dt_last_crawled_article:
                    print('[Crawl] Article date {} recent than {} for {}'\
                    .format(dt_article.isoformat(), dt_last_crawled_article.isoformat(), type(self).__name__))
                    all_new_articles.append(article)
            except:
                print('[Crawl] Failed article {} for {}'.format(link, type(self).__name__))

        return all_new_articles

# click based
class CcnLatest:

    def __init__(self, driver):
        # options = Options()
        # options.headless = True
        self.driver = driver
        self.baseLink = 'https://www.ccn.com/'
        self.source = News.CCN
        print("[Crawl] Headless Firefox Initialized for {}".format(type(self).__name__))

    def get_latest_articles(self, dt_last_crawled_article, latest=200):

        print('[Crawl] Adding maximum {} articles recent than {} for {}'.format(latest, dt_last_crawled_article.isoformat(), self.source.value))
        all_new_articles = []
        dt_last_crawled_article = Utils.dt_make_aware(dt_last_crawled_article)
        nr_articles = 0

        self.driver.get(self.baseLink)

        while nr_articles < latest:
            self.driver.find_element_by_css_selector('.posts-row .load-more-btn').click()
            p1 = pq(self.driver.page_source)
            select = '.content .main .page-content .content .posts-row article'
            nr_articles = len(p1(select))

        print('[Crawl] Articles to be crawled: {} for {}'.format(nr_articles, self.source.value))

        for i in range(nr_articles):
            try:
                article, link = self.crawl_article(i, p1, select)
                dt_article = dateparser.parse(article[NewsDoc.DATE.value])
                dt_article = Utils.dt_make_aware(dt_article)

                if dt_last_crawled_article < dt_article:
                    all_new_articles.append(article)
                    print('[Crawl] Article {} added with date-time: {} for {}'.format(link, dt_article.isoformat(), self.source.value))
            except:
                print('[Crawl] Article {} failed for {}'.format(link, self.source.value))
        return all_new_articles

    # get index article from page p1
    def crawl_article(self, index, p1, article_selector):
        article = {}
        link = p1(article_selector).eq(index)('header .entry-title a').attr('href')
        self.driver.get(link)
        p2 = pq(self.driver.page_source)
        
        # date 
        dt_raw = p2('meta').filter(lambda x: pq(this).attr('property')=='article:published_time').eq(0).attr('content')
        dt_article = Utils.dt_make_aware(dateparser.parse(dt_raw))
        dt_article_iso = dt_article.isoformat()
        
        # author
        author = p2('.page-content article footer .post-author').text()
        
        # title
        title = p2('meta').filter(lambda x: pq(this).attr('property')=='og:title').eq(0).attr('content')
        
        # content
        selContent = 'article .entry-content p' 
        nPar = len(p2(selContent))
        content = "\n".join([p2(selContent).eq(tt).text() for tt in range(nPar)])

        article[NewsDoc.DATE.value] = dt_article_iso
        article[NewsDoc.AUTHOR.value] = author.replace("AUTHOR", "")
        article[NewsDoc.TITLE.value] = title
        article[NewsDoc.CONTENT.value] = content
        article[NewsDoc.LANG.value] = Lang.ENG.value
        article[NewsDoc.SOURCE.value] = self.source.value
        article[NewsDoc.LINK.value] = link
        return article, link  

# click based
class CoinnessLatest:
    def __init__(self, driver):
        # options = Options()
        # options.headless = True
        self.driver = driver
        self.baseLink = 'https://kr.coinness.com'
        self.source = News.COINNESS
        print("[Crawl] Headless Firefox Initialized for {}".format(type(self).__name__))

    def get_latest_articles(self, dt_last_crawled_article, latest=200):

        dt_last_crawled_article = Utils.dt_make_aware(dt_last_crawled_article)
        print('[Crawl] Adding maximum {} articles recent than {} for {}'\
        .format(latest, dt_last_crawled_article.isoformat(), self.source.value))
        all_new_articles = []

        self.driver.get(self.baseLink)
        nArticles = 0
        while nArticles < latest:
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
            element = self.driver.find_elements_by_css_selector('.container .nno .fl .news-list .read-more')[0]
            element.click()
            p = pq(self.driver.page_source)
            nArticles = len(p('.home-container .day .newscontainer li a'))
            print('[Crawl] Page has {} articles for {}'.format(nArticles, self.source.value))
        
        p1 = pq(self.driver.page_source)
        sel = '.news-list .home-container .day .newscontainer li .content > a'
        nrArticles = len(p1(sel))
        links = p1(sel)

        for i in range(nrArticles):
            try:
                link = links.eq(i).attr('href')
                
                link = self.baseLink + link
                article = self.crawl_article(link)
                dt_article = dateparser.parse(article[NewsDoc.DATE.value])
                dt_article = Utils.dt_make_aware(dt_article)

                if dt_last_crawled_article < dt_article:
                    all_new_articles.append(article)
                    print('[Crawl] Article {} added with date-time: {} for {}'\
                    .format(link, dt_article.isoformat(), self.source.value))
            except:
                print('[Crawl] Article {} failed for {}'.format(link, self.source.value))
        
        print('[Crawl] Done crawling for {}, crawled {} articles'\
                .format(type(self).__name__, len(all_new_articles)))
        return all_new_articles

    def crawl_article(self, link):
        article = {}
        self.driver.get(link)
        p2 = pq(self.driver.page_source)
                
        # date 
        datel = p2('.news-container .content-wrap .time').text().split(' ')
        hour = datel[0]
        year = "".join(list(filter(lambda x: x.isdigit(), datel[1])))
        month = "".join(list(filter(lambda x: x.isdigit(), datel[2])))
        day = "".join(list(filter(lambda x: x.isdigit(), datel[3])))
        dt_article_iso = dateparser.parse('{} {} {} {}'.format(year, month, day, hour)).isoformat()
        # author
        author = str(None)
        # title
        title = p2('.news-container .title').text()
        # content
        #nPar = len(p2('.news-container .content-wrap .content'))
        content = p2('.news-container .content-wrap .content').text()
        #content = "\n".join([p2('.news-container .content-wrap content font').eq(i).text() for i in range(nPar)])
        article[NewsDoc.DATE.value] = dt_article_iso
        article[NewsDoc.AUTHOR.value] = author
        article[NewsDoc.TITLE.value] = title
        article[NewsDoc.CONTENT.value] = content
        article[NewsDoc.LINK.value] = link
        article[NewsDoc.LANG.value] = Lang.KO.value
        article[NewsDoc.SOURCE.value] = self.source.value
        return article

# scroll based - almost done -
class CoinDeskKoreaLatest:
    
    def __init__(self, driver):
        # options = Options()
        # options.headless = True
        self.driver = driver
        self.baseLink = 'https://www.coindeskkorea.com/news/'
        self.source = News.COINKO
        print("[Crawl] Headless Firefox Initialized for {}".format(type(self).__name__))

    # look in the latest news only
    def get_latest_articles(self, dt_last_crawled_article, latest=200):

        print('[Crawl] Adding maximum {} articles recent than {} for {}'\
        .format(latest, dt_last_crawled_article.isoformat(), self.source.value))
        all_new_articles = []

        self.driver.get(self.baseLink)
        nArticles = 0
        while nArticles < latest:
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(2)
            p1 = pq(self.driver.page_source)
            articleSel = '.art_list_main .et_pb_posts .et_pb_section .entry-title a'
            articles = p1(articleSel)
            nArticles = len(articles)
            print('[Crawl] Page has {} articles for {}'.format(nArticles, self.source.value))

        p1 = pq(self.driver.page_source)

        dt_last_crawled_article = Utils.dt_make_aware(dt_last_crawled_article)

        select = '.art_list_main .et_pb_posts .et_pb_section .entry-title a'
        nr_articles = len(p1(select))
        print('[Crawl] Articles to be crawled: {} from {}'.format(nr_articles, self.source.value))

        for i in range(nr_articles):
            try:
                link = p1('.art_list_main .et_pb_posts .et_pb_section .entry-title a').eq(i).attr('href')
                article = self.crawl_article(link)
                dt_article = dateparser.parse(article[NewsDoc.DATE.value])
                dt_article = Utils.dt_make_aware(dt_article)

                if dt_last_crawled_article < dt_article:
                    all_new_articles.append(article)
                    print('[Crawl] Article {} added with date-time: {} for {}'\
                    .format(link, dt_article.isoformat(), self.source.value))
            except:
                print('[Crawl] Article {} failed for {}'.format(link, self.source.value))
        
        print('[Crawl] Done crawling for {}, crawled {} articles'\
                .format(type(self).__name__, len(all_new_articles)))
        return all_new_articles
    
    # get index article from page p1
    def crawl_article(self, link):
        article = {}
        self.driver.get(link)
        p2 = pq(self.driver.page_source)
        
        # date 
        dt_raw = p2('meta').filter(lambda x: pq(this).attr('property')=='article:published_time').eq(0).attr('content')
        dt_article = Utils.dt_make_aware(dateparser.parse(dt_raw))
        dt_article_iso = dt_article.isoformat()
        # author
        author = str(p2('#main-content .meta_group_1 .author').text())
        # title
        title = str(p2('#main-content .post_head_wrap .entry-title').text())
        # content
        selContent = '#main-content .et_pb_row_inner .et_pb_column .et_pb_cpt_text p'
        nPar = len(p2(selContent))
        content = "\n".join([p2(selContent).eq(tt).text() for tt in range(nPar)])

        article[NewsDoc.DATE.value] = dt_article_iso
        article[NewsDoc.AUTHOR.value] = author
        article[NewsDoc.TITLE.value] = title
        article[NewsDoc.CONTENT.value] = content
        article[NewsDoc.LINK.value] = link
        article[NewsDoc.LANG.value] = Lang.KO.value
        article[NewsDoc.SOURCE.value] = self.source.value
        return article
    
# page based - done
class BlockChainHubLatest:

    def __init__(self, driver):
        # options = Options()
        # options.headless = True
        self.driver = driver
        self.baseLink = 'https://www.blockchainhub.kr/'
        self.source = News.BLKHUB
        print("[Crawl] Headless Firefox Initialized for {}".format(type(self).__name__))

    def get_latest_articles(self, dt_last_crawled_article):

        print("[Crawl] Getting latest articles ({}) for {}"\
        .format(dt_last_crawled_article.isoformat(), type(self).__name__))
        dt_last_crawled_article = Utils.dt_make_aware(dt_last_crawled_article)
        generator_pages = self.get_pages()
        all_new_articles = []

        while True:
            page = next(generator_pages)
            articles = self.get_articles(page, dt_last_crawled_article)
            all_new_articles += articles
            print('[Crawl] New articles crawled {} for {}'.format(len(articles), type(self).__name__))
            
            if len(articles) == 0:
                print('[Crawl] Done crawling for {}, crawled {} articles'\
                .format(type(self).__name__, len(all_new_articles)))
                break
        
        return all_new_articles

    # generate pages
    def get_pages(self):
        self.driver.get(self.baseLink)
        
        for i in range(1, 40):
            try:
                link = self.baseLink + 'bbs/board.php?bo_table=news&page=' + str(i)
                print('[Crawl] Getting page {} for {}'.format(link, type(self).__name__))
                self.driver.get(link)
                #with open(resFolder + '/page' + str(i) + '.txt', 'w', encoding='utf-8') as f:
                yield self.driver.page_source
            except GeneratorExit:
                print('[Crawl] Page generator ended for {}'.format(type(self).__name__))
                return
            except:
                print('[Craw] Failed page {} for {}'.format(link, type(self).__name__))

    def get_articles(self, page, dt_last_crawled_article):
        p1 = pq(page)
        nrArticles = len(p1('.board-list .list-wrap form .list-webzine .list-media .list-item .media-body .media-heading a'))
        print('[Crawl] Crawling {} articles (a page) for {}'.format(nrArticles,  type(self).__name__))
        all_new_articles = []

        for i in range(nrArticles):
            try:
                article = {}
                link = p1('.board-list .list-wrap form .list-webzine .list-media .list-item .media-body .media-heading a').eq(i).attr('href')
                self.driver.get(link)
                print('[Crawl] Getting article {} for {}'.format(link, type(self).__name__))
                p2 = pq(self.driver.page_source)
                # date
                date_raw = str(p2('.panel-heading .pull-right').text())
                date_parsed = dateparser.parse(date_raw)
                # it means is a recent article, take only the first 2 chars
                if date_parsed == None:
                    date_parsed = dateparser.parse(date_raw[:2])

                if date_parsed == None:
                    continue
                
                dt_article_iso = date_parsed.isoformat()
                # author
                author = str(None)
                # title
                title = str(p2('.view-wrap h1').eq(0).text())
                # content
                cssSeelector = '.view-wrap .view-content p'
                nPar = len(p2(cssSeelector))
                content = "\n".join([p2(cssSeelector).eq(i).text() for i in range(nPar)])

                article[NewsDoc.DATE.value] = dt_article_iso
                article[NewsDoc.AUTHOR.value] = author
                article[NewsDoc.TITLE.value] = title
                article[NewsDoc.CONTENT.value] = content
                article[NewsDoc.LINK.value] = link
                article[NewsDoc.LANG.value] = Lang.KO.value
                article[NewsDoc.SOURCE.value] = self.source.value

                dt_article = dateparser.parse(article[NewsDoc.DATE.value])
                dt_article = Utils.dt_make_aware(dt_article)
                if dt_article > dt_last_crawled_article and len(content) > 0:
                    print('[Crawl] Article date {} recent than {} for {}'\
                    .format(dt_article.isoformat(), dt_last_crawled_article.isoformat(), type(self).__name__))
                    all_new_articles.append(article)
            except:
                print('[Craw] Failed article {} for {}'.format(link, type(self).__name__))
        return all_new_articles

# page based - done
class TokenPostLatest:

    def __init__(self, driver):
        self.driver = driver
        self.baseLink  = 'https://tokenpost.kr'
        self.source = News.TOKENPOST
        self.baseLinks = [
            ('https://tokenpost.kr/business?page=', 324),
            ('https://tokenpost.kr/regulation?page=', 43),
            ('https://tokenpost.kr/technology?page=', 48),
            ('https://tokenpost.kr/investing?page=', 115),
            ('https://tokenpost.kr/insights?page=', 214),
            ('https://tokenpost.kr/people?page=', 33),
            ('https://tokenpost.kr/briefing?page=', 32)]
        print("[Crawl] Headless Firefox Initialized for {}".format(type(self).__name__))

    def get_latest_articles(self, dt_last_crawled_article):
        print("[Crawl] Getting latest articles ({}) for {}"\
        .format(dt_last_crawled_article.isoformat(), type(self).__name__))
        dt_last_crawled_article = Utils.dt_make_aware(dt_last_crawled_article)
        generator_pages = self.get_pages(3)
        all_new_articles = []

        while True:
            page = next(generator_pages)
            articles = self.get_articles(page, dt_last_crawled_article)
            all_new_articles += articles
            print('[Crawl] New articles crawled {} for {}'.format(len(articles), type(self).__name__))
            
            if len(articles) == 0:
                print('[Crawl] Done crawling for {}, crawled {} articles'\
                .format(type(self).__name__, len(all_new_articles)))
                break
        
        return all_new_articles

    def get_pages(self, pages_limit):

        for (link, _) in self.baseLinks:
            for i in range(1, pages_limit + 1):
                try:
                    print('[Crawl] Getting page {} for {}'.format(link + str(i), type(self).__name__))
                    self.driver.get(link + str(i))
                    yield self.driver.page_source
                except GeneratorExit:
                    print('[Crawl] Page generator ended for {}'.format(type(self).__name__))
                    return
                except:
                    print('[Craw] Failed page {} for {}'.format(link, type(self).__name__))

    def get_articles(self, page, dt_last_crawled_article):
        p1 = pq(page)
        select = '#wrap .articleListWrap' 
        nrArticles = len(p1(select))
        print('[Crawl] Crawling {} articles (a page) for {}'.format(nrArticles,  type(self).__name__))
        all_new_articles = []

        for i in range(nrArticles):
            try:
                article = {}
                link = self.baseLink + p1(select).eq(i)('.articleListTitle a').attr('href')
                self.driver.get(link)
                print('[Crawl] Getting article {} for {}'.format(link, type(self).__name__))
                p2 = pq(self.driver.page_source)
               
                # date 
                dt_article_iso = dateparser.parse(p1(select).eq(i)('.articleListDate').text()).isoformat()
                # author
                author = p2('meta').filter(lambda x: pq(this).attr('name')=='Author').eq(0).attr('content')
                # title
                title = str(p2('#wrap #commonArea #view .viewContent .viewContentArticle .viewArticleTitle .ArticleBigTitle').text())
                # content
                selContent = '#wrap #commonArea #view .viewContent .viewContentArticle .viewArticle' 
                nPar = len(p2(selContent))
                content = "\n".join([p2(selContent).eq(tt).text() for tt in range(nPar)])

                article[NewsDoc.DATE.value] = dt_article_iso
                article[NewsDoc.AUTHOR.value] = author
                article[NewsDoc.TITLE.value] = title
                article[NewsDoc.CONTENT.value] = content
                article[NewsDoc.LINK.value] = link
                article[NewsDoc.LANG.value] = Lang.KO.value
                article[NewsDoc.SOURCE.value] = self.source.value

                dt_article = dateparser.parse(article[NewsDoc.DATE.value])
                dt_article = Utils.dt_make_aware(dt_article)
                if dt_article > dt_last_crawled_article:
                    print('[Crawl] Article date {} recent than {} for {}'\
                    .format(dt_article.isoformat(), dt_last_crawled_article.isoformat(), type(self).__name__))
                    all_new_articles.append(article)
            except:
                print('[Crawl] Failed article {} for {}'.format(link, type(self).__name__))
        return all_new_articles


if __name__ == '__main__':

    news_obj_latest = []
    tweets_obj_latest = []
    driver = Utils.get_firefox_driver()

    blk_media_latest = BlockmediaLatest(driver)
    news_obj_latest.append(blk_media_latest)

    blkChain = BlockChainHubLatest(driver)
    news_obj_latest.append(blkChain)

    token_post = TokenPostLatest(driver)
    news_obj_latest.append(token_post)

    coin_desk_korea = CoinDeskKoreaLatest(driver)
    news_obj_latest.append(coin_desk_korea)

    ccn_latest = CcnLatest(driver)
    news_obj_latest.append(ccn_latest)

    coinness = CoinnessLatest(driver)
    news_obj_latest.append(coinness)

    desk_coin = DeskCoinLatest(driver)
    news_obj_latest.append(desk_coin)

    moneyneynet = MoneyNetLatest(driver)
    news_obj_latest.append(moneyneynet)

   
    # articles = blk_media_latest.get_latest_articles(dt)
    # articles = blkChain.get_latest_articles(dt)
    # articles = token_post.get_latest_articles(dt)
    # articles = coin_desk_korea.get_latest_articles(dt, 200)
    # articles = ccn_latest.get_latest_articles(dt, 300)
    # articles = coinness.get_latest_articles(dt, 400)
    # articles = desk_coin.get_latest_articles(dt, 200)
    # articles = moneyneynet.get_latest_articles(dt)
    # articles = twitter.get_latest_articles(dt)
    
    # get es instance
    elastic = ElasticS(clean_instance=False)
    for twitterChannel in Twitter:
        for twitterLink in TwitterLinks:
            if twitterChannel.name == twitterLink.name:
                twitter = TwitterLatest(driver, twitterChannel, twitterLink.value)
                tweets_obj_latest.append(twitter)
    # for obj_latest in news_obj_latest:
    #     latest_news_index = elastic.search_last_indexed(news_source=obj_latest.source)
    #     dt = Utils.get_latest_date(latest_news_index)
    #     latest_articles_site = obj_latest.get_latest_articles(driver, dt)
    #     elastic.index_news_from_memory(latest_articles_site, obj_latest.source)
    #     elastic.search_last_indexed(news_source=obj_latest.source)
    
    for obj_latest_twitter in tweets_obj_latest:
        last_indexed = elastic.search_last_indexed(index_to_seacrh=Elastic.ELASTIC_INDEX_TWEETS,\
                                                    source=obj_latest_twitter.source,
                                                    size=1)
        dt = Utils.get_latest_date(last_indexed)

        latest_tweets = obj_latest_twitter.get_latest_articles(dt)
        elastic.index_tweets_from_memory(latest_tweets, obj_latest_twitter.source)
        elastic.search_last_indexed(index_to_seacrh=Elastic.ELASTIC_INDEX_TWEETS,\
                                    source=obj_latest_twitter.source,
                                    size=5)
    # 
    # #elastic.search_last_news(news_source=News.CCN)

    # for a in articles:
    #     print(json.dumps(a, indent=True))