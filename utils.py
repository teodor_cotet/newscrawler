import pytz
import dateparser

from enum import Enum

from selenium import webdriver
import selenium
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options

class News(Enum):
    BLKHUB = 'blockchainhub'
    BLKMEDIA = 'blockmedia'
    CCN = 'ccn'
    COINKO = 'coindeskkorea'
    COINNESS = 'coinness'
    DESKCOIN = 'deskcoin'
    MONEYNEY = 'moneynet'
    TOKENPOST = 'tokenpost'

class Elastic(Enum):
    ELASTIC_PORT = 9200
    ELASTIC_HOST = "localhost"
    ELASTIC_INDEX_NEWS = 'news-index'
    ELASTIC_INDEX_TWEETS = 'tweets-index'
    ELASTIC_DOC_TYPE_NEWS = 'news'
    ELASTIC_DOC_TYPE_TWEET = 'tweet'

class NewsDoc(Enum):
    SOURCE = 'source'
    DATE = 'date'
    AUTHOR = 'author'
    TITLE = 'title'
    LANG = 'language'
    CONTENT = 'content'
    LINK = 'link'

class Lang(Enum):
    ENG = 'eng'
    KO = 'ko'

# twitter channels
class Twitter(Enum):
    BTCINFORUM = 'bitcoinforum'
    BTCZMAGAZINE = 'bitcoin_magazine'
    #BTCTALK = 'bitcointalk'
    BTCTN = 'btctn'
    BTCWIRES = 'btc_wires'
    COINDESK = 'coindesk'
    COINTELEGRAPH = 'cointelegraph'
    NEWSBTC = 'newsbtc'

class TwitterLinks(Enum):
    BTCINFORUM = 'https://twitter.com/BitcoinForumCom'
    BTCZMAGAZINE = 'https://twitter.com/BitcoinMagazine'
    BTCTALK = 'https://twitter.com/Bitcoin_TT'
    #BTCTN = 'https://twitter.com/BTCTN'
    BTCWIRES = 'https://twitter.com/btc_wires'
    COINDESK = 'https://twitter.com/coindesk'
    COINTELEGRAPH = 'https://twitter.com/Cointelegraph'
    NEWSBTC = 'https://twitter.com/newsbtc'

class TweetDoc(Enum):
    SOURCE = 'source' # channel on wich it was posted
    DATE = 'date'
    AUTHOR = 'author' # original author channel (may be a retweet)
    COMMENTS = 'comments'
    RETWEETS = 'retweets'
    LIKES = 'likes'
    LANG = 'language'
    CONTENT = 'content'
    LINK = 'link'

class Utils():

    @staticmethod
    def dt_make_aware(dt):
        if dt.tzinfo is None or dt.tzinfo.utcoffset(dt) is None:
            return dt.replace(tzinfo=pytz.utc)
        return dt
    
    @staticmethod
    def get_latest_date(article):
        return dateparser.parse(article["hits"]["hits"][0]["_source"]["date"])
    
    @staticmethod
    def get_firefox_driver():
        options = Options()
        options.headless = True
        return webdriver.Firefox(options=options)
