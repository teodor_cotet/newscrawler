from selenium import webdriver
import selenium
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options
from pyquery import PyQuery as pq
from dateutil import parser
from datetime import datetime
from dateutil.tz import tzutc
from os import listdir
from os.path import isfile, join
import time


from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

class Coindeskkorea:
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.baseLink = 'https://www.coindeskkorea.com/news/'
        print("Headless Firefox Initialized")

    def getPages(self, resFolder):
        self.driver.get(self.baseLink)
        lastWrite = 0
        while True:
            try:
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                time.sleep(1)
                p1 = pq(self.driver.page_source)
                articleSel = '.art_list_main .et_pb_posts .et_pb_section .entry-title a'
                articles = p1(articleSel)
                nArticles = len(articles)
                print(nArticles)
                if nArticles - lastWrite > 100: 
                    lastWrite = nArticles
                    print('write')
                    with open(resFolder + '/page' + str(nArticles) + '.txt', 'w', encoding='utf-8') as f:
                        f.write(self.driver.page_source)
            except:
                print('fail')
        self.driver.close()
        self.driver.quit()

    def getArticles(self, dirPages, outFolder):
        onlyfiles = [f for f in listdir(dirPages) if isfile(join(dirPages, f))]
        j = 0
        fileo = 'page1317.txt'
        with open(dirPages + '/' + fileo, 'r', encoding='utf-8') as f:
            s = f.read()
        p1 = pq(s)
        
        nrArticles = len(p1('.art_list_main .et_pb_posts .et_pb_section .entry-title a'))
        print('nrArticles {}'.format(nrArticles))
            
        for i in range(nrArticles):
            try:
                link = p1('.art_list_main .et_pb_posts .et_pb_section .entry-title a').eq(i).attr('href')
                print(i, link)
                self.driver.get(link)
                p2 = pq(self.driver.page_source)
                
                with open(outFolder + '/' + str(j) + '.txt', 'w', encoding='utf-8') as f:
                    # date 
                    #print(p2('#main-content .meta_group_2 .published').eq(0).html().split('|')[0][len('Registration:'):-2])
                    #date = p2('#main-content .meta_group_2 .published font').eq(0).text()[len('Registration:'):-2]
                    #parseDate = parser.parse(date).isoformat()
                    parseDate = p2('meta').filter(lambda x: pq(this).attr('property')=='article:published_time').eq(0).attr('content')
                    f.write(str(parseDate))
                    f.write('\n')
                    # author
                    f.write(str(p2('#main-content .meta_group_1 .author').text()))
                    f.write('\n')
                    # title
                    f.write(str(p2('#main-content .post_head_wrap .entry-title').text()))
                    f.write('\n')
                    # link
                    f.write(link)
                    f.write('\n')
                    # content
                    cssSeelector = '#main-content .et_pb_row_inner .et_pb_column .et_pb_cpt_text p'
                    nPar = len(p2(cssSeelector))
                    content = "\n".join([p2(cssSeelector).eq(tt).text() for tt in range(nPar)])
                    f.write(content)
                    j += 1
            except:
                print("article {} failed".format(i))
                    
        self.driver.close()
        self.driver.quit()

if __name__ == "__main__":
    ck = Coindeskkorea()
    #ck.getPages('pages')
    ck.getArticles('pages', 'parsed_articles')
