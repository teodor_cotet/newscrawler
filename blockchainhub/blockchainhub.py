from selenium import webdriver
import selenium
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options
from pyquery import PyQuery as pq
import dateutil.parser
from datetime import datetime
from dateutil.tz import tzutc
from os import listdir
from os.path import isfile, join
import dateparser

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

class BlockChainHub:
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.baseLink = 'https://www.blockchainhub.kr/'
        print("Headless Firefox Initialized")

    def getPages(self, resFolder):
        self.driver.get(self.baseLink)
        
        for i in range(1, 933):
            try:
                link = self.baseLink + 'bbs/board.php?bo_table=news&page=' + str(i)
                print(link)
                self.driver.get(link)
                with open(resFolder + '/page' + str(i) + '.txt', 'w', encoding='utf-8') as f:
                    f.write(self.driver.page_source)
            except:
                print('failed page ' + str(i))
        self.driver.close()        
        self.driver.quit()

    def getArticles(self, dirPages, outFolder):
        onlyfiles = [f for f in listdir(dirPages) if isfile(join(dirPages, f))]
        j = 0
        
        for ff in range(len(onlyfiles)):
            print('page {}'.format(ff))
            inFile = onlyfiles[ff]
            with open(dirPages + '/' + inFile, 'r', encoding='utf-8') as f:
                s = f.read()
            p1 = pq(s)
            nrArticles = len(p1('.board-list .list-wrap form .list-webzine .list-media .list-item .media-body .media-heading a'))
            print('nrArticles {}'.format(nrArticles))
            
            for i in range(nrArticles):
                try:
                    link = p1('.board-list .list-wrap form .list-webzine .list-media .list-item .media-body .media-heading a').eq(i).attr('href')
                    print(j)
                    self.driver.get(link)
                    p2 = pq(self.driver.page_source)
                    # link = self.baseLink + str(href)
                    # self.driver.get(link)
                    
                    with open(outFolder + '/' + str(j) + '.txt', 'w', encoding='utf-8') as f:
                        # date 
                        date_raw = str(p2('.panel-heading .pull-right').text())
                        date_parsed = dateparser.parse(date_raw)
                        # it means is a recent article, take only the first 2 chars
                        if date_parsed == None:
                            date_parsed = dateparser.parse(date_raw[:2])

                        if date_parsed == None:
                            continue
                        
                        f.write(date_parsed.isoformat())
                        f.write('\n')
                        # author
                        f.write(str(None))
                        f.write('\n')
                        # title
                        f.write(str(p2('.view-wrap h1').eq(0).text()))
                        f.write('\n')
                        # link -check
                        f.write(str(link))
                        f.write('\n')
                        # content
                        cssSeelector = '.view-wrap .view-content p'
                        nPar = len(p2(cssSeelector))
                        content = "\n".join([p2(cssSeelector).eq(i).text() for i in range(nPar)])
                        f.write(content)
                        j += 1
                except:
                    print('failed ' + str(i))
        self.driver.close()
        self.driver.quit()

if __name__ == "__main__":
    bch = BlockChainHub()
    #bch.getPages('pages')
    bch.getArticles('pages', 'parsed_articles')
