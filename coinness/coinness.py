from selenium import webdriver
import selenium
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options
from pyquery import PyQuery as pq
import dateutil.parser
from datetime import datetime
from dateutil.tz import tzutc
from os import listdir
from os.path import isfile, join

import dateparser

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

class Coinness:
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.baseLink = 'https://kr.coinness.com'
        print("Headless Firefox Initialized")

    def getPages(self, resFolder):
        self.driver.get(self.baseLink)
        lastWrite = 0
        while True:
            try:
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
                element = self.driver.find_elements_by_css_selector('.container .nno .fl .news-list .read-more')[0]
                element.click()
                p = pq(self.driver.page_source)
                nArticles = len(p('.home-container .day .newscontainer li a'))
                print(nArticles)
                if nArticles - lastWrite > 600:
                    print('write')
                    lastWrite = nArticles
                    with open(resFolder + '/page' + str(nArticles) + '.html', 'w', encoding='utf-8') as f:
                        f.write(self.driver.page_source)
            except:
                print('failed')

    def getArticles(self, dirPages, outFolder):
        onlyfiles = [f for f in listdir(dirPages) if isfile(join(dirPages, f))]
        infile = 'page28000.txt'        
        j = 0

        with open(dirPages + '/' + infile, 'r', encoding='utf-8') as f:
            s = f.read()
        
        p1 = pq(s)
        nrArticles = len(p1('.news-list .home-container .day .newscontainer li .content a'))
        links = p1('.news-list .home-container .day .newscontainer li .content a')
        print(nrArticles)
        for i in range(nrArticles):
            try:
                link = links.eq(i).attr('href')
                if link == "javascript:void(0)":
                    continue
                
                link = self.baseLink + link
                print(i, link)
                self.driver.get(link)
                p2 = pq(self.driver.page_source)
                
                with open(outFolder + '/' + str(j) + '.txt', 'w', encoding='utf-8') as f:
                    # date 
                    datel = p2('.news-container .content-wrap .time').text().split(' ')
                    hour = datel[0]
                    year = "".join(list(filter(lambda x: x.isdigit(), datel[1])))
                    month = "".join(list(filter(lambda x: x.isdigit(), datel[2])))
                    day = "".join(list(filter(lambda x: x.isdigit(), datel[3])))
                    datet = dateparser.parse('{} {} {} {}'.format(year, month, day, hour)).isoformat()
                    f.write(datet)
                    f.write('\n')
                    # author
                    f.write(str(None))
                    f.write('\n')
                    # title
                    f.write(p2('.news-container .title').text())
                    f.write('\n')
                    # link
                    f.write(str(link))
                    f.write('\n')
                    # content
                    #nPar = len(p2('.news-container .content-wrap .content'))
                    content = p2('.news-container .content-wrap .content').text()
                    #content = "\n".join([p2('.news-container .content-wrap content font').eq(i).text() for i in range(nPar)])
                    f.write(content)
                    j += 1
            except:
                print('failed for ' + str(j))

        self.driver.quit()

if __name__ == "__main__":
    coinness = Coinness()
    coinness.getArticles('pages', 'parsed_articles')
    #dk.getLinks('res.html')
    #coinness.getPages('pages')
