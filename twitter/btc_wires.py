from selenium import webdriver
import selenium
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options
from pyquery import PyQuery as pq
import dateutil.parser
from datetime import datetime
from dateutil.tz import tzutc
from os import listdir
from os.path import isfile, join
import os
import time 

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

class Twitter:
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.baseLink = 'https://twitter.com/btc_wires'
        print("Headless Firefox Initialized")

    def getArticles(self, outFolder):
        self.driver.get(self.baseLink)
        lastTweetCrawled = 0
        while True:
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                time.sleep(5)
                p1 = pq(self.driver.page_source)
                tweetSel = '.ProfileTimeline ol li .tweet'
                tweets = p1(tweetSel)
                nTweets = len(tweets)
                print(nTweets)
                while lastTweetCrawled < nTweets:
                    try:
                        tweet = tweets.eq(lastTweetCrawled)
                        with open(outFolder + '/' + str(lastTweetCrawled) + '.txt', 'w', encoding='utf-8') as f:
                            # date 
                            strTime = tweet('.content .time ._timestamp').attr('data-time')
                            strTimeIso = str(datetime.utcfromtimestamp(int(strTime)).isoformat())
                            f.write(strTimeIso)
                            f.write('\n')
                            # author / tweet channel
                            username = str(tweet('.content .stream-item-header .username').text())
                            f.write(username)
                            f.write('\n')
                            # links
                            link = tweet('.content .stream-item-header > small a').attr('href')
                            print('https://twitter.com' + str(link))
                            f.write('https://twitter.com' + str(link))
                            f.write('\n')
                            # comments
                            selComments = tweet('.content .stream-item-footer .ProfileTweet-actionList .ProfileTweet-action--reply .ProfileTweet-actionCount .ProfileTweet-actionCountForPresentation').eq(0)
                            if len(selComments.text()) == 0:
                                nrComments = 0
                            else:
                                nrComments = int(selComments.text())
                            f.write(str(nrComments))
                            f.write('\n')

                            # retweets
                            selReTweets = tweet('.content .stream-item-footer .ProfileTweet-actionList .ProfileTweet-action--retweet .ProfileTweet-actionCount .ProfileTweet-actionCountForPresentation').eq(0)
                            #print(selReTweets.text())
                            if len(selReTweets.text()) == 0:
                                retweets = 0
                            else:
                                retweets = int(selReTweets.text())
                            f.write(str(retweets))
                            f.write('\n')

                            # # likes
                            selLikes = tweet('.content .stream-item-footer .ProfileTweet-actionList .ProfileTweet-action--favorite .ProfileTweet-actionCount .ProfileTweet-actionCountForPresentation').eq(0)
                            if len(selLikes.text()) == 0:
                                likes = 0
                            else:
                                likes = int(selLikes.text())
                            f.write(str(likes))
                            f.write('\n')

                            #no title for tweets
                            # content
                            cssSeelector = '.content .js-tweet-text-container p'
                            nPar = len(tweet(cssSeelector))
                            content = "".join([tweet(cssSeelector).eq(i).text() for i in range(nPar)])
                            cc = " ".join(content.split('\n'))
                            goodContent = cc.replace("@ ", "@").replace("# ", "#")
                            f.write(goodContent)
                            print('tweet ok ' + str(lastTweetCrawled))
                            lastTweetCrawled += 1
                    except:
                        # delete partially crawled tweet and skip the tweet
                        print('tweet failed ' + str(lastTweetCrawled))
                        os.remove(outFolder + '/' + str(lastTweetCrawled) + '.txt') 
                        lastTweetCrawled += 1
        self.driver.close()
        self.driver.quit()

if __name__ == "__main__":
    twitter = Twitter()
    #bch.getPages('pages')
    twitter.getArticles('btc_wires')
