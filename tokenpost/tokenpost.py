from selenium import webdriver
import selenium
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options
from pyquery import PyQuery as pq
import dateutil.parser
from datetime import datetime
from dateutil.tz import tzutc
from os import listdir
from os.path import isfile, join

import dateparser

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import time

class TokenPost:
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.baseLink  = 'https://tokenpost.kr'
        self.baseLinks = [
            ('https://tokenpost.kr/business?page=', 324),
            ('https://tokenpost.kr/regulation?page=', 43),
            ('https://tokenpost.kr/technology?page=', 48),
            ('https://tokenpost.kr/investing?page=', 115),
            ('https://tokenpost.kr/insights?page=', 214),
            ('https://tokenpost.kr/people?page=', 33),
            ('https://tokenpost.kr/briefing?page=', 32)]
        print("Headless Firefox Initialized")

    def getPages(self, resFolder):
        for (link, nPages) in self.baseLinks:
            for i in range(1, nPages + 1):
                #try:
                    self.driver.get(link + str(i))
                    with open(resFolder + '/page_' + link[22:] + '_' + str(i) + '.html', 'w', encoding='utf-8') as f:
                        f.write(self.driver.page_source)
                #except:
                #    print('failed')
        self.driver.quit()

    def getArticles(self, dirPages, outFolder):
        onlyfiles = [f for f in listdir(dirPages) if isfile(join(dirPages, f))]
        j = 0

        for inFile in onlyfiles:
            with open(dirPages + '/' + inFile, 'r', encoding='utf-8') as f:
                s = f.read()
            
            p1 = pq(s)
            select = '#wrap .articleListWrap' 
            nrArticles = len(p1(select))

            for i in range(nrArticles):
                try:
                    link = self.baseLink + p1(select).eq(i)('.articleListTitle a').attr('href')
                    print(i, link)
                    self.driver.get(link)
                    p2 = pq(self.driver.page_source)
                    
                    with open(outFolder + '/' + str(j) + '.txt', 'w', encoding='utf-8') as f:
                        # date 
                        #parseDate = p2('meta').filter(lambda x: pq(this).attr('property')=='article:published_time').eq(0).attr('content')
                        parseDate = dateparser.parse(p1(select).eq(i)('.articleListDate').text()).isoformat()
                        f.write(str(parseDate))
                        f.write('\n')
                        # author
                        author = p2('meta').filter(lambda x: pq(this).attr('name')=='Author').eq(0).attr('content')
                        f.write(str(author))
                        f.write('\n')
                        # title
                        f.write(str(p2('#wrap #commonArea #view .viewContent .viewContentArticle .viewArticleTitle .ArticleBigTitle').text()))
                        f.write('\n')
                        # link
                        f.write(str(link))
                        f.write('\n')
                        # content
                        selContent = '#wrap #commonArea #view .viewContent .viewContentArticle .viewArticle' 
                        nPar = len(p2(selContent))
                        content = "\n".join([p2(selContent).eq(tt).text() for tt in range(nPar)])
                        f.write(str(content))
                        j += 1
                except:
                    print('failed for article {}'.format(j))
        self.driver.quit()

if __name__ == "__main__":
    tokenPost = TokenPost()
    tokenPost.getArticles('pages', 'parsed_articles')
    #dk.getLinks('res.html')
    #tokenPost.getPages('pages')
