from selenium import webdriver
import selenium
import selenium.webdriver.support.ui as ui
from selenium.webdriver.firefox.options import Options
from pyquery import PyQuery as pq
import dateutil.parser
from datetime import datetime
from dateutil.tz import tzutc
from os import listdir
from os.path import isfile, join

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import time

class Moneynet:
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.baseLink = 'https://www.moneynet.co.kr/index.php?mid=info_board&category=20297&page='
        print("Headless Firefox Initialized")

    def getPages(self, resFolder):
        
        for i in range(1, 232):
            try:
                self.driver.get(self.baseLink + str(i))
                time.sleep(2)
                with open(resFolder + '/page' + str(i) + '.txt', 'w', encoding='utf-8') as f:
                    f.write(self.driver.page_source)
            except:
                print('failed')

    def getArticles(self, dirPages, outFolder):
        onlyfiles = [f for f in listdir(dirPages) if isfile(join(dirPages, f))]
        j = 0

        for inFile in onlyfiles:
            with open(dirPages + '/' + inFile, 'r', encoding='utf-8') as f:
                s = f.read()
            
            p1 = pq(s)
            select = 'tbody tr .title a'
            nrArticles = len(p1(select))

            for i in range(nrArticles):
                try:
                    link = p1(select).eq(i).attr('href')
                    print(i, link)
                    self.driver.get(link)
                    #time.sleep(4)
                    p2 = pq(self.driver.page_source)
                    
                    with open(outFolder + '/' + str(j) + '.txt', 'w', encoding='utf-8') as f:
                        # date 
                        parseDate = p2('meta').filter(lambda x: pq(this).attr('property')=='article:published_time').eq(0).attr('content')
                        print(parseDate)
                        f.write(str(parseDate))
                        f.write('\n')
                        # author
                        f.write(str(p1('tbody tr .author').eq(i).text()))
                        f.write('\n')
                        # title
                        f.write(str(p2('.board_read .section_bottom h1').text()))
                        f.write('\n')
                        # content
                        nPar = len(p2('.section_wrap .read_body p'))
                        content = "\n".join([p2('.section_wrap .read_body p').eq(tt).text() for tt in range(nPar)])
                        f.write(str(content))
                        j += 1
                except:
                   print('failed for article {}'.format(j))
        self.driver.quit()

if __name__ == "__main__":
    moneynet = Moneynet()
    moneynet.getArticles('pages', 'parsed_articles')
    #dk.getLinks('res.html')
    #moneynet.getPages('pages')
